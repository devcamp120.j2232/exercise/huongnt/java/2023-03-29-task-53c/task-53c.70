package models;

import interfaces.Resizable;

public class ResizableCircle implements Resizable {
    public double radius;
    

    public ResizableCircle(double radius) {
        this.radius = radius;
    }


    @Override
    public double resize(int percent) {
        // TODO Auto-generated method stub
        radius *= percent/100.0;
        return radius;
    }


    @Override
    public String toString() {
        return "ResizableCircle [radius=" + radius + "]";
    }
    
}
